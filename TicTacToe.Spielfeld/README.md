# Tic-Tac-Toe <!-- omit in toc -->

Kennen Sie [Tic-Tac-Toe](https://de.wikipedia.org/wiki/Tic-Tac-Toe), auch "Drei gewinnt" genannt? Tic-Tac-Toe ist ein Brettspiel für zwei Personen. Auf dem Spielfeld aus 3x3 Feldern werden abwechselnd Spielsteine gesetzt. Der Anziehende hat die "Kreuze", der Nachziehende die "Kringel". Wer zuerst drei seiner Spielsteine in Reihe bringt (senkrecht, waagerecht oder diagonal) hat gewonnen.

Diese kleine Javalin-Anwendung stellt zwei Spielern ein Spielbrett für Tic-Tac-Toe zur Verfügung. Mit einem Klick auf das entsprechende Feld wird ein Spielstein gesetzt.

Ich erkläre Ihnen, wie ich bei der Entwicklung der Anwendung vorgegangen bin.

- [Die Darstellung des Spielbretts](#die-darstellung-des-spielbretts)
- [Der Versand von HTML-Requests](#der-versand-von-html-requests)
  - [Mit Gradle das Projekt aufsetzen](#mit-gradle-das-projekt-aufsetzen)
  - [JavaScript für `XMLHttpRequest`](#javascript-f%C3%BCr-xmlhttprequest)
  - [Die Anwendung generiert den Tabelleninhalt](#die-anwendung-generiert-den-tabelleninhalt)
- [Das Spielbrett für Tic-Tac-Toe](#das-spielbrett-f%C3%BCr-tic-tac-toe)
  - [Erweiterungsvorschläge](#erweiterungsvorschl%C3%A4ge)
  - [Alles ist im Fluss](#alles-ist-im-fluss)

## Die Darstellung des Spielbretts

Sind Sie auch noch so unerfahren mit HTML und CSS wie ich? Wie in aller Welt bekommt man damit ein Spielfeld für Tic-Tac-Toe hin? Eine Tabelle könnte ein guter Ansatz für ein Spielfeld aus 3x3 Feldern sein, aber wie genau mache ich das?

Wozu gibt es das Internet!? Ein kurze Suche fördert mir eine rein in HTML, CSS und JavaScript umgesetzte Tic-Tac-Toe-Anwendung zutage [[Link](https://codepen.io/anon/pen/rebYNw)], die genau das kann, was ich gerne nachbauen möchte und mir als Vorbild für meine Umsetzung dient. Allerdings soll nicht JavaScript, sondern der Javalin-Server die Anwendungslogik stellen. Der Browser soll auf gar keinen Fall Anwendungslogik umsetzen! 

Ich extrahiere mir den HTML- und den CSS-Code und lege in in einer Datei namens `index.html` ab. Das sieht im Browser angeschaut doch schon anständig aus und ist ein Anfangspunkt. Was mir vor allem gefällt: Das Spielfeld, das ich mit der Maus anfahre, verändert seine Farbe; `hover` ist dafür verantwortlich. So schwer verständlich ist der HTML-Code und der CSS-Code gar nicht!

~~~ html
<html>
<head> 
<title>Tic-Tac-Toe</title>    
</head>
<body>
  <table id='board'>
    <tr>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td"></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </table>
</body>  
</html>

<style>
* {
    margin: 0; 
    padding: 0; 
  }
  
  table { 
    border-collapse: collapse; 
    border-spacing: 0; 
  }
  
  #board { 
    padding: 0px; 
    margin: 0 auto; 
  }
  
  #board tr td { 
    width: 80px; 
    height: 80px; 
    border: 1px solid #1c1c1c; 
    font-family: Helvetica;  
    font-size: 30px; 
    text-align: center; 
  }
   
  #board tr td:hover { 
    background: #e4e4e4; 
    cursor: pointer; 
  }
}
</style>
~~~

Aber wie bekomme ich es hin, wenn ich auf ein Spielfeld klicke, dass dann in dem Feld etwas erscheint?

## Der Versand von HTML-Requests

Bevor es losgeht, muss das Projekt mit Gradle eingerichtet werden.

### Mit Gradle das Projekt aufsetzen

Es ist Zeit, den Javalin-Code aufzusetzen. Ich erstelle mir den Ordner `tictactoe`. Mit dem Windows-Kommando `mkdir` (_make directory_) erstellt man einen Ordner (man spricht auch von einem Verzeichnis), und mit dem Kommando `cd` (_change directory_) wechselt man in das angegebene Verzeichnis.

~~~ shell
C:\Users\Dominikus\GitTHM\pis\SoSe2019>mkdir tictactoe

C:\Users\Dominikus\GitTHM\pis\SoSe2019>cd tictactoe

C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe>
~~~

Gradle hilft mir bei der Initialisierung eines Projektverzeichnisses und fragt unterwegs ein paar Dinge ab. Wann immer möglich wähle ich die Default-Einstellung.

~~~ shell
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe>gradle init

Select type of project to generate:
  1: basic
  2: cpp-application
  3: cpp-library
  4: groovy-application
  5: groovy-library
  6: java-application
  7: java-library
  8: kotlin-application
  9: kotlin-library
  10: scala-library
Enter selection (default: basic) [1..10] 6

Select build script DSL:
  1: groovy
  2: kotlin
Enter selection (default: groovy) [1..2] 1

Select test framework:
  1: junit
  2: testng
  3: spock
Enter selection (default: junit) [1..3] 1

Project name (default: tictactoe):
Source package (default: tictactoe):

BUILD SUCCESSFUL in 31s
2 actionable tasks: 2 executed
~~~

Schauen Sie sich in der Ordnerstruktur um. Sie sehen, dass Gradle zwei Beispieldateien angelegt hat. (Das Kommando `dir` listet die Inhalte eines Verzeichnisses, _directory_, auf. Die Option `/s` zeigt auch den Inhalt der Unterverzeichnisse an, `/b` stellt die Auflistung kompakt zusammen.)

~~~ shell
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe>dir src /s /b
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe\src\main
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe\src\test
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe\src\main\java
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe\src\main\resources
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe\src\main\java\tictactoe
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe\src\main\java\tictactoe\App.java
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe\src\test\java
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe\src\test\resources
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe\src\test\java\tictactoe
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe\src\test\java\tictactoe\AppTest.java
~~~

> Wenn Sie mit Git arbeiten, denken Sie daran, eine .gitignore-Datei anzulegen! All die automatisch und temporär angelegten Dateien, die Gradle erzeugt (wie z.B. die kompilierten Java-Dateien), haben nichts in einem Git-Repository verloren. Git dient hauptsächlich zur Verwaltung des Quellcodes in `/src`.

Wenn Sie mögen, können Sie die Beispieldatei kompilieren und ausführen lassen.

~~~ shell
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe>gradle run

> Task :run
Hello world.

BUILD SUCCESSFUL in 3s
2 actionable tasks: 2 executed
~~~

Nachdem das alles abgeschlossen ist, überschreibe ich die `App.java` mit dem ["Hello Word"-Code](https://javalin.io/documentation#getting-started) aus der Javalin-Doku und passe Gradle für die Arbeit mit Javalin an. Mein `build.gradle` fällt wieder schlank aus:

~~~ gradle
plugins {
    id 'java'
    id 'application'
}

repositories {
    jcenter()
}

dependencies {
    compile 'io.javalin:javalin:2.8.0'
    compile 'org.slf4j:slf4j-simple:1.7.25'
}

mainClassName = 'tictactoe.App'
~~~

### JavaScript für `XMLHttpRequest`

Also: Wie kann eine Javalin-Applikation auf einen Mausklick ins Tic-Tac-Toe-Spielfeld reagieren? Ich recherchiere wieder und lerne bald, dass es ohne JavaScript nicht geht. Ich kann die `<td>`-Tags mit einem `onclick`-Attribut versehen und als Aufruf eine JavaScript-Funktion angeben. Als Parameter gebe ich die Nummer der Position auf dem Spielfeld an.

~~~ html
  <table id='board'>
    <tr>
      <td onclick="sendMove(0)"></td>
      <td onclick="sendMove(1)"></td>
      <td onclick="sendMove(2)"></td>
    </tr>
    <tr>
      <td onclick="sendMove(3)"></td>
      <td onclick="sendMove(4)"></td>
      <td onclick="sendMove(5)"></td>
    </tr>
    <tr>
      <td onclick="sendMove(6)"></td>
      <td onclick="sendMove(7)"></td>
      <td onclick="sendMove(8)"></td>
    </tr>
  </table>
~~~

Die Funktion `sendMove()` muss einen HTTP-Request erzeugen. In der `index.html`-Datei kommt ein `<script>`-Tag dazu für den JavaScript-Code.

~~~ html
<script type="text/javascript">
  var http = new XMLHttpRequest();
  
  function sendMove(field) {
      http.open('GET', 'move?pos=' + field);
      http.send();
  }
</script>
~~~

Die Datei `App.java` bekommt die Grundausstattung, um die HTTP-Anfrage entgegen nehmen zu können.

~~~ java
package tictactoe;

import io.javalin.Javalin;

public class App {
    public static void main(String[] args) {
        Javalin app = Javalin.create()
            .enableStaticFiles("/public")
            .start(7000);
        app.get("/move", ctx -> {
            int input = Integer.parseInt(ctx.queryParam("pos"));
            ctx.result(input);
        });
    }
}
~~~

Ich arbeite in dieser Phase der Entwicklung sehr viel mit den Entwicklungswerkzeugen des Browsers Chrome. So kann ich sehen, ob der Request wie gedacht erzeugt wird und ob er bei der Anwendung ankommt.

Noch wird mit der Rückgabe `ctx.result(...)` auf den Request im Browser nichts gemacht. Wieder recherchiere ich. Ich kann mit der Rückgabe im Browser Teile des HTML-Codes (genauer: des DOM) ersetzen. Cool! Den JavaScript-Code muss ich dafür ein wenig erweitern.

~~~ html
<script type="text/javascript">
  var http = new XMLHttpRequest();
  
  function sendMove(field) {
    http.open('GET', 'move?pos=' + field);
     http.send();
  }

  http.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById('board').innerHTML = this.responseText;
    }
  }
</script>
~~~

> Tatsächlich habe ich an dieser Stelle viel rumgespielt und ausprobiert, um zu verstehen, wie JavaScript und die Javalin-Anwendung HTTP-Request senden, verarbeiten und die Rückgaben einsetzen. Ganz so gradlinig, wie hier beschrieben, ist es nicht gewesen.

### Die Anwendung generiert den Tabelleninhalt

Es ist Zeit, sich an die Umsetzung der Spiellogik zu machen, zu finden in `T3.java`. Ich nutze die `toString`-Methode, um den HTML-Code für das Spielfeld zu generieren. Das Spielfeld ist als eindimensionales Feld in `board` abgelegt.

~~~ java
    @Override
    public String toString() {
        char[] sym = {'O', ' ', 'X'};
        char[] repr = new char[9];
        for(int i = 0; i < board.length; i++) repr[i] = sym[board[i] + 1];
        return 
            "<tr>\n" +
            String.format("  <td onclick=\"sendMove(0)\">%c</td>\n", repr[0]) +
            String.format("  <td onclick=\"sendMove(1)\">%c</td>\n", repr[1]) +
            String.format("  <td onclick=\"sendMove(2)\">%c</td>\n", repr[2]) +
            "</tr>\n" +
            "<tr>\n" +
            String.format("  <td onclick=\"sendMove(3)\">%c</td>\n", repr[3]) +
            String.format("  <td onclick=\"sendMove(4)\">%c</td>\n", repr[4]) +
            String.format("  <td onclick=\"sendMove(5)\">%c</td>\n", repr[5]) +
            "</tr>\n" +
            "<tr>\n" +
            String.format("  <td onclick=\"sendMove(6)\">%c</td>\n", repr[6]) +
            String.format("  <td onclick=\"sendMove(7)\">%c</td>\n", repr[7]) +
            String.format("  <td onclick=\"sendMove(8)\">%c</td>\n", repr[8]) +
            "</tr>\n";
    }
~~~

## Das Spielbrett für Tic-Tac-Toe

Wenn Sie bis hierhin alles einigermaßen verstanden haben, dann sollte sich der Rest des Codes gut erschließen lassen. Wenn Sie nicht sofort alles verstehen, unterschätzen Sie nicht, wieviel Zeit ich selber aufbringen musste, bis die Anwendung so weit war. Die Auseinandersetzung mit neuen Technologien und Programmierwerkzeugen ist anfangs oft zeitaufwändig.

### Erweiterungsvorschläge

Ich habe ein paar Vorschläge, wie Sie die Anwendung erweitern können:

* Wenn Sie das Spielbrett in einem zweiten Reiter (oder Tab) Ihres Browsers öffnen, werden Sie feststellen, dass es sich um den Spielstand aus dem ersten Reiter handelt. Zwei ganz verschiedene Ideen dazu:
  1. Schön wäre es, wenn sich in jedem Reiter ein ganz neues, unabhängiges Tic-Tac-Toe-Spielen ließe. 
  2. Oder: Sobald Sie in dem einen Reiter einen Zug machen, wird der Spielstand im anderen Reiter aktualisiert. So bleiben die Spielstände synchron.
* Es ist natürlich etwas umständlich, mit der `toString`-Methode die HTML-Repräsentation des Spielfelds zu generieren. Nutzen Sie stattdessen [Views and Templates](https://javalin.io/documentation#views-and-templates), das ist elegant
* Nutzen Sie die Möglichkeit der [Validierung](https://javalin.io/documentation#validation) von an den Server übertragenen Daten.
* Sobald drei Spielsteine in Reihe sind, werden die betreffenden drei Spielsteine rot eingefärbt.
* Erlauben Sie dem Anwender bzw. der Anwenderin gegen den Computer Tic-Tac-Toe zu spielen!

### Alles ist im Fluss

In der Klasse `T3` finden Sie in der Methode `threeInARow` etwas ungewöhnlichen Code. Sie sehen in der Variablen `rows` alle Kombinationen kodiert, welche Spielfeldpositionen überprüft werden müssen, um festzustellen, ob drei Spielsteine in Reihe sind.

~~~ java
boolean threeInARow() {
    int i = 8;
    int[][] rows = {{0,1,2}, {3,4,5}, {6,7,i}, // horizontal
                    {0,3,6}, {1,4,7}, {2,5,6}, // vertical
                    {0,4,i}, {2,4,6}};         // diagonal
    return Arrays.stream(rows).parallel().anyMatch(row -> {
                int sum = Arrays.stream(row).map(pos -> board[pos]).sum();
                return Math.abs(sum) == 3;
    });
}
~~~

Das `return` arbeitet mit Strömen (_streams_) -- der Quellcode lässt sich fast in natürliche Sprache übersetzen: Gehe alle `rows` gleichzeitig (`parallel`) durch und schaue, ob eine dieser Reihen einen Treffer liefert (`anyMatch(row -> ...)`), wo sich die Summe `sum` der Spielbrettsteine an den gegebenen Positionen (`.map(pos -> board[pos]).sum()`) im Absolutwert als drei ergibt (`Math.abs(sum) == 3`).

> Es ist neben der Arbeit mit Javalin ein Ziel, dass Sie diese moderne Art der Java-Programmierung beherrschen lernen.